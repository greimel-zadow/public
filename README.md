# Fabian Greimel & Frederick Zadow: Downloads for our joint projects

## Understanding Housing Wealth Effects: Debt, Home Ownership and the Lifecycle

[paper](https://gitlab.com/greimel-zadow/public/builds/artifacts/master/raw/housing-wealth-effects/paper/paper.pdf?job=compile_pdfs)

[slides](https://gitlab.com/greimel-zadow/public/builds/artifacts/master/raw/housing-wealth-effects/slides/slides.pdf?job=compile_pdfs)

